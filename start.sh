#!/bin/bash
app="frysite"
docker rm ${app}
docker build -t ${app} .
docker run  --name ${app} -p 8000:80 ${app}
