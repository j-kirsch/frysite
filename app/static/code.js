var saturation, contrast, brightness, iterations, quality, image, can, ctx;

var bulges = [];

var dotSize = 10;

function onload(){
	saturation = document.getElementById("saturation");
	contrast = document.getElementById("contrast");
	brightness = document.getElementById("brightness");
	noiseFactor = document.getElementById("noiseFactor");
	iterations = document.getElementById("iterations");
	quality = document.getElementById("quality");
	image = document.getElementById("fried-image");

	can = document.getElementById("bulge-can");
    ctx = can.getContext("2d");
    resizeCan();

    window.addEventListener("wheel", resizeBulge);
}

function drawBulges() {
    ctx.clearRect(0, 0, can.width, can.height);
    var scale = [image.naturalWidth / image.width];

    for(i in bulges) {
        var pos = {x: bulges[i].x/scale, y: bulges[i].y/scale};

        ctx.beginPath();
        ctx.arc(pos.x, pos.y, dotSize, 0, 2 * Math.PI, false);
        ctx.fillStyle = 'white';
        ctx.fill();
        ctx.lineWidth = 3;
        ctx.strokeStyle = '#555';
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(pos.x, pos.y, bulges[i].r/scale, 0, 2 * Math.PI, false);
        ctx.lineWidth = 1;
        ctx.strokeStyle = '#f00';
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(pos.x, pos.y, bulges[i].s*bulges[i].r/scale, 0, 2 * Math.PI, false);
        ctx.lineWidth = 1;
        ctx.strokeStyle = '#0f0';
        ctx.stroke();
    }
}

function resizeBulge(e) {

    function norm(a, b) {
        return Math.sqrt((a.x-b.x)**2 + (a.y-b.y)**2)
    }

    var pos = getMousePos(e);
    var scale = [image.naturalWidth / image.width];
    pos = {x: pos.x*scale, y: pos.y*scale};

    for(i in bulges) {
        if(norm(pos, bulges[i]) < dotSize) {
            if(!e.ctrlKey) {
                bulges[i].r -= Math.sign(e.deltaY) * 10;
            } else {
                bulges[i].s -= Math.sign(e.deltaY) * 0.1;
                bulges[i].s = Math.max(0.1, Math.min(bulges[i].s, 1))
            }
            drawBulges();
            e.preventDefault();
        }
    }
}

function refresh() {

    var bulgeString = ""
    for(i in bulges) {
        b = bulges[i];
        if(i>0) bulgeString += ";";
        bulgeString += String(Math.round(b.x))+":"+String(Math.round(b.y))+":"+String(b.r)+":"+String(b.s);
    }
    console.log(bulgeString)

	var xhttp = new XMLHttpRequest();
	var requestString = "/update/?file="+image.alt+
		"&saturation="+saturation.value+
		"&contrast="+contrast.value+
		"&brightness="+brightness.value+
		"&noiseFactor="+noiseFactor.value+
		"&iterations="+iterations.value+
		"&quality="+quality.value;
	if(bulges.length>0) requestString += "&bulges="+bulgeString;
	xhttp.open("GET", requestString, true);

	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			image.src = "/" + xhttp.responseText+"?token="+new Date();
		}
	}

	xhttp.send();
}

function resizeCan() {
    can.width = image.width;
    can.height = image.height;
}

function  getMousePos(evt) {
  var rect = can.getBoundingClientRect();
  return {
    x: (evt.clientX - rect.left),
    y: (evt.clientY - rect.top)
  }
}

function placeBulge(evt) {
    var pos = getMousePos(evt);

    // Assuming uniform scale
    var scale = [image.naturalWidth / image.width];
    bulges.push({x: pos.x*scale, y: pos.y*scale, r: 100, s: 0.5});

    drawBulges();
}
