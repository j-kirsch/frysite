# FrySite
Includes a script to deepfry images and a little flask server for an HTML interface.

Requires Python3 for the development server or Docker for the
dockerized version.

To run the dev server execute runserver.sh from the root directory,
it will run on port 5000.
To run the dockerized version (under development) execute the
start.sh script, the server will run on port 8000.

![alt text](https://i.makeagif.com/media/4-11-2018/RCyX0U.gif "Pepe")
